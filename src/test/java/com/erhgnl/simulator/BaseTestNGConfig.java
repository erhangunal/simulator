package com.erhgnl.simulator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;

public class BaseTestNGConfig {

    private final Logger LOGGER = LoggerFactory.getLogger(BaseTestNGConfig.class);

    @BeforeSuite()
    public void beforeSuite() {
        LOGGER.info("@BeforeSuite");
    }

    @AfterSuite()
    public void afterSuite() {
        LOGGER.info("@AfterSuite");
    }

    @BeforeTest()
    public void beforeTest() {
        LOGGER.info("@BeforeTest");
    }

    @AfterTest()
    public void afterTest() {
        LOGGER.info("@AfterTest");
    }
}
