package com.erhgnl.simulator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

public class SimulatorTest extends BaseTestNGConfig {

    private final Logger LOGGER = LoggerFactory.getLogger(SimulatorTest.class);

    @Test
    public void simulatorTest(){
        analyzeMethodTest();
    }

    private void analyzeMethodTest(){
        Simulator simulator = new Simulator();
        List<String> input = new ArrayList<>();

        input.add("Resources are 5000 meters away\n");
        input.add("Hero has 1000 hp\n");
        input.add("Hero attack is 10\n");
        input.add("Bug is Enemy\n");
        input.add("Lion is Enemy\n");
        input.add("Zombie is Enemy\n");
        input.add("Bug has 50 hp\n");
        input.add("Bug attack is 2\n");
        input.add("Lion has 100 hp\n");
        input.add("Lion attack is 15\n");
        input.add("Zombie has 300 hp\n");
        input.add("Zombie attack is 7\n");
        input.add("There is a Zombie at position 1681\n");
        input.add("There is a Bug at position 276\n");
        input.add("There is a Bug at position 489\n");
        input.add("There is a Lion at position 1527\n");
        input.add("There is a Lion at position 2865\n");
        input.add("There is a Zombie at position 3523\n");

        LOGGER.info("INPUT: \n" + input.toString());
        LOGGER.info("OUTPUT: \n" + simulator.analyzeInput(input));
    }
}
