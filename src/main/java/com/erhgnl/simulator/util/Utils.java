package com.erhgnl.simulator.util;

public class Utils {

    private Utils() {
    }

    public static String buildMessage(String str, String... params) {
        int counter 		=	0;
        StringBuilder sb	=	new StringBuilder(str);

        for (String param : params) {
            String key	=	"${" + (counter++) + "}";
            int start	=	sb.indexOf(key);
            int end		=	start + key.length();
            if (start > -1) {
                sb.replace(start, end, param);
            }
        }
        return sb.toString();
    }

    public static int getNumericValue(String in){
        int intValue = 0;
        String str = in.replaceAll(Constants.REGEX_ALFA_NUMERIC, "");

        if (str != null && ! str.equals("")){
            intValue = Integer.parseInt(str);
        }
        return intValue;
    }
}
