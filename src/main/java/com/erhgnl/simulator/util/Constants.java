package com.erhgnl.simulator.util;

public class Constants {

    private Constants() {
    }

    public static final String RSRC_KYWRD = "Resource";
    public static final String HERO_KYWRD = "Hero";
    public static final String ENMY_KYWRD = "Enemy";
    public static final String HP_KYWRD = "hp";
    public static final String ATTCK_KYWRD = "attack";
    public static final String PSTN_KYWRD = "position";

    public static final String START_MSG = "${0} started journey with ${1} HP!";
    public static final String ACTION_MSG = "${0} defeated ${1} with ${2} HP remaining";
    public static final String SUCCESS_MSG = "${0} Survived!";
    public static final String FAILURE_MSG = "${0} is Dead!! Last seen at position ${1}!!";

    public static final String REGEX_ALFA_NUMERIC = "[^\\d.]";
    public static final String SEPERATOR = " ";
}
