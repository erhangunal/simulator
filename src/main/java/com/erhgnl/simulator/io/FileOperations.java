package com.erhgnl.simulator.io;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class FileOperations {

    private static final Logger LOGGER = LoggerFactory.getLogger(FileOperations.class);

    public List<String> readerFile(String filePath){
        ArrayList<String> lines = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(filePath))) {
            String sCurrentLine;
            while ((sCurrentLine = br.readLine()) != null) {
                lines.add(sCurrentLine);
            }
            LOGGER.info("Input File Line Size:{}",lines.size());
            LOGGER.info("Read Input Done.");
        } catch (IOException e) {
            LOGGER.error("ERROR - filePath: " + filePath + " does not exist!!" );
        }
        return lines;
    }

    public void writerFile(String filePath, StringBuilder content){
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(filePath))) {
            bw.write(content.toString());
            LOGGER.info("Write Output Done.");
        } catch (IOException e) {
            LOGGER.error("ERROR - File Write:", e);
        }
    }

}
