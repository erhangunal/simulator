package com.erhgnl.simulator.model;

public class BaseProperty {

    private int hp;
    private int attack;

    public BaseProperty(int hp, int attack) {
        this.hp = hp;
        this.attack = attack;
    }

    public BaseProperty() {
    }

    public int getHp() {
        return hp;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public int getAttack() {
        return attack;
    }

    public void setAttack(int attack) {
        this.attack = attack;
    }

    @Override
    public String toString() {
        return "BaseProperty{" +
                "hp=" + hp +
                ", attack=" + attack +
                '}';
    }
}
