package com.erhgnl.simulator.model;

public class Hero extends Live {

    public Hero(int hp, int attack, int position) {
        super(hp, attack, position);
        this.setEnemy(false);
    }

    public Hero() {
    }

    @Override
    public String toString() {
        return "Hero{" +
                ", live=" + super.toString() +
                '}';
    }
}
