package com.erhgnl.simulator.model;

import java.util.Comparator;

public class Enemy extends Live implements Comparator<Enemy> {

    private String name;

    public Enemy(int hp, int attack, int position, String name) {
        super(hp, attack, position);
        this.setEnemy(true);
        this.name = name;
    }

    public Enemy() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int compare(Enemy o1, Enemy o2) {
        return o1.getPosition().compareTo(o2.getPosition());
    }

    @Override
    public String toString() {
        return "EnemyClass{" +
                ", live=" + super.toString() +
                ", name='" + name +
                '}';
    }
}
