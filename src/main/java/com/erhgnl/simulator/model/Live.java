package com.erhgnl.simulator.model;

public class Live extends BaseProperty{

    private Integer position;
    private Boolean isEnemy;

    public Live(int hp, int attack, int position) {
        super(hp,attack);
        this.position = position;
    }

    public Live() {
    }

    public Integer getPosition() {
        return position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    public Boolean getEnemy() {
        return isEnemy;
    }

    public void setEnemy(Boolean enemy) {
        isEnemy = enemy;
    }

    @Override
    public String toString() {
        return "Live{" +
                "baseProperty=" + super.toString() +
                ", position=" + position +
                ", isEnemy=" + isEnemy +
                '}';
    }
}
