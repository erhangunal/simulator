package com.erhgnl.simulator;

import com.erhgnl.simulator.io.FileOperations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class MainClass {

    private static final Logger LOGGER = LoggerFactory.getLogger(MainClass.class);

    public static void main(String args[]) {

        if (args != null && args.length == 2) {
            String inputFile = args[0];
            LOGGER.info("InputFile:" + inputFile);

            String outputFile = args[1];
            LOGGER.info("OutputFile:" + outputFile);

            simulateOperations(inputFile,outputFile);

        } else {
            LOGGER.error("Expected arguments not found!");
        }
    }

    private static void simulateOperations(String inputFile, String outputFile){
        FileOperations fileOperations = new FileOperations();
        Simulator simulator = new Simulator();
        List<String> inputList = fileOperations.readerFile(inputFile);
        if (inputList != null && !inputList.isEmpty()){
            StringBuilder result = simulator.analyzeInput(inputList);
            fileOperations.writerFile(outputFile,result);
        }
    }
}
