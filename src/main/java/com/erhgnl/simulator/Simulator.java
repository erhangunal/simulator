package com.erhgnl.simulator;

import com.erhgnl.simulator.util.Constants;
import com.erhgnl.simulator.model.BaseProperty;
import com.erhgnl.simulator.model.Enemy;
import com.erhgnl.simulator.model.Hero;
import com.erhgnl.simulator.util.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

public class Simulator {

    private static final Logger LOGGER = LoggerFactory.getLogger(Simulator.class);

    public StringBuilder analyzeInput(List<String> lines){
        int resourcePosition = 0;
        Hero hero = new Hero();
        LinkedHashMap<String, BaseProperty> enemyPropertyMap = new LinkedHashMap<>();
        ArrayList<Enemy> enemies = new ArrayList<>();

        for (String in : lines){
            int intValue = Utils.getNumericValue(in);

            if (in.contains(Constants.RSRC_KYWRD)) {
                resourcePosition = intValue;
            } else if (in.contains(Constants.HERO_KYWRD)){
                heroOperations(hero,in,intValue);
            } else if (in.contains(Constants.ENMY_KYWRD)){
                enemyPropertyMap.put(in.split(Constants.SEPERATOR)[0], new BaseProperty());
            } else {
                enemyOperations(enemyPropertyMap,in,intValue,enemies);
            }
        }

        enemies.sort((Enemy o1, Enemy o2) -> o1.getPosition().compareTo(o2.getPosition()));

        LOGGER.info("Alayze Done.");
        return simulate(enemies, hero,resourcePosition);
    }

    private void heroOperations(Hero hero, String in, int intValue){
        if (in.contains(Constants.HP_KYWRD)){
            hero.setHp(intValue);
        } else if (in.contains(Constants.ATTCK_KYWRD)){
            hero.setAttack(intValue);
        }
    }

    private void enemyOperations(LinkedHashMap<String, BaseProperty> enemyPropertyMap, String in, int intValue, ArrayList<Enemy> enemies){
        if (in.contains(Constants.HP_KYWRD) || in.contains(Constants.ATTCK_KYWRD)){
            String enemyName = in.split(Constants.SEPERATOR)[0];
            BaseProperty baseProperty = enemyPropertyMap.get(enemyName);
            if (in.contains(Constants.HP_KYWRD)) {
                baseProperty.setHp(intValue);
            } else if (in.contains(Constants.ATTCK_KYWRD)){
                baseProperty.setAttack(intValue);
            }
        } else if (in.contains(Constants.PSTN_KYWRD)){
            String enemyName = in.split(Constants.SEPERATOR)[3];
            BaseProperty baseProperty = enemyPropertyMap.get(enemyName);
            enemies.add(generateEnemy(enemyName,baseProperty,intValue));
        }
    }


    private Enemy generateEnemy(String name, BaseProperty baseProperty, int position){
        return new Enemy(baseProperty.getHp(),baseProperty.getAttack(),position,name);
    }



    private StringBuilder simulate(ArrayList<Enemy> enemies, Hero hero, int resourcePosition){
        StringBuilder result = new StringBuilder();
        boolean isHeroDead = false;

        result.append(Utils.buildMessage(Constants.START_MSG,Constants.HERO_KYWRD, String.valueOf(hero.getHp())));
        result.append("\n");

        for (Enemy enemy : enemies){

            if (enemy.getPosition() > resourcePosition) {
                LOGGER.info("Ahead of " + enemy.getName() + " resource position !!");
                break;
            }

            double enemyAttackCount = (double) enemy.getHp() / hero.getAttack();
            double volunteerAttackCount = (double) hero.getHp() / enemy.getAttack();

            hero.setPosition(enemy.getPosition());
            int remainingHP;

            if (enemyAttackCount > volunteerAttackCount){
                remainingHP = calculateRemainingHP(enemy.getHp(),volunteerAttackCount, hero.getAttack());

                result.append(Utils.buildMessage(Constants.ACTION_MSG,enemy.getName(),Constants.HERO_KYWRD,String.valueOf(remainingHP)));
                result.append("\n");

                result.append(Utils.buildMessage(Constants.FAILURE_MSG,Constants.HERO_KYWRD, hero.getPosition().toString()));

                isHeroDead = true;
                break;

            } else {
                remainingHP = calculateRemainingHP(hero.getHp(),enemyAttackCount,enemy.getAttack());

                result.append(Utils.buildMessage(Constants.ACTION_MSG,Constants.HERO_KYWRD,enemy.getName(),String.valueOf(remainingHP)));
                result.append("\n");

                hero.setHp(remainingHP);
            }
        }

        if (! isHeroDead){
            result.append(Utils.buildMessage(Constants.SUCCESS_MSG,Constants.HERO_KYWRD));
        }

        LOGGER.debug(result.toString());
        LOGGER.info("Simulate Done.");
        return result;
    }

    private int calculateRemainingHP(int currentHP, double attackCount, int attack){
        return currentHP - ((int)Math.ceil(attackCount) * attack);
    }
}
